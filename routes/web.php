<?php
use Illuminate\Support\Facades\App;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin','AdminController@index');

//Route::get('/admin/pages','AdminController@pages');
Route::get('/admin/pages','AdminController@pages');
Route::get('/admin/pages', 'Company\CompanyController@index')->name('company');

Route::get('/language', function () {
    App::setlocale('pt-br');
    //dd(App::getLocale());
    //return view('welcome');
    return redirect('/home');
});

Route::get('/language/pt-br', function () {
    App::setlocale('pt-br');
    //dd(App::getLocale());
    return view('home');
    //return redirect('/home');
});

Route::get('/language/en', function () {
    App::setlocale('en');
    //dd(App::getLocale());
    return view('home');
    //return redirect('/home');
});

Route::get('/Company', 'Company\CompanyController@index')->name('company');
Route::get('initConfig', 'Company\CompanyController@initConfig')->name('company.initConfig');
Route::put('preparationConfig', 'Company\CompanyController@preparationConfig')->name('company.preparationConfig');
Route::put('configCompany', 'Company\CompanyController@configCompany')->name('company.configCompany');
