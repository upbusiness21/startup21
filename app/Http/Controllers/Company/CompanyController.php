<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
	protected $repo;

	public function __construct()
	{

	}

	public function index()
	{
	//dd('Aqui');
		
		//dd($company->configured==1);
		//if ($company->configured==1) {
			//Vai para a página do dashboard se já estiver configurado
			//return view('company.index');
			//return view('company.index', compact('simulation'));
		//} else {
	  //	//Redireciona para iniciar a configuração 
	  return redirect()->route('company.initConfig');
		//}
		
	}

	public function edit()
	{
		dd('company.edit');
		return view('company.edit');
	}

	public function update(Request $request)
	{
		dd('company.update');
		$company = $this->repo->update($request->toArray(), auth()->user()->id);


		// TODO: Refatorar
		$logo = $request->file('photo');

		if (!empty($logo)) {
			$ext = ['jpg', 'png', 'jpeg'];

			if ($logo->isValid() && in_array($logo->extension(), $ext)) {
				$image = Image::make($logo);

				$image->fit(300, 300, function ($constraint) {
					$constraint->aspectRatio();
				});
				//$file = $request->file('file');

				$novoNome = bin2hex(openssl_random_pseudo_bytes(8)) . '.' . $logo->extension();
				//dd($novoNome);
				//Storage::put('public/companies/' . $logo->getClientOriginalName(), (string) $image->encode());
				//$company->logo = $logo->getClientOriginalName();
				$logo->storeAs('public/companies/', $novoNome);
				//Storage::put('public/companies/' . $novoNome, (string) $image->encode());
				//$company->logo = $novoNome;
				$company->logo = $novoNome;
				$company->save();
			}
		}

		notification('Empresa atualizada com sucesso!');

		return redirect()->route('company.edit');
	}

	public function initConfig (Request $request) {
		//dd('company.initConfig');
		//$company = $this->repo->update($request->toArray(), auth()->user()->id);
		//$simulation = auth()->user()->simulation;
		return view('company.config');
	}


	public function preparationConfig(Request $request)
	{
		//dd('company.preparationConfig');
		//$company = $this->repo->update($request->toArray(), auth()->user()->id);
		//$simulation = auth()->user()->simulation;

		//dd($company,$request);
		// TODO: Refatorar
		//$logo = $request->file('photo');

		/* if (!empty($logo)) {
			$ext = ['jpg', 'png', 'jpeg'];

			if ($logo->isValid() && in_array($logo->extension(), $ext)) {
				$image = Image::make($logo);

				$image->fit(300, 300, function ($constraint) {
					$constraint->aspectRatio();
				});
				//$file = $request->file('file');

				$novoNome = bin2hex(openssl_random_pseudo_bytes(8)) . '.' . $logo->extension();
				//dd($novoNome);
				//Storage::put('public/companies/' . $logo->getClientOriginalName(), (string) $image->encode());
				//$company->logo = $logo->getClientOriginalName();
				$logo->storeAs('public/companies/', $novoNome);
				//Storage::put('public/companies/' . $novoNome, (string) $image->encode());
				//$company->logo = $novoNome;
				$company->logo = $novoNome;
				$company->save();
			}
		} */

		//Vai para a página das compras iniciais
		//return view('company.shop', compact('simulation'));
		//return view('company.shop', compact('simulation','company'));
		return view('company.shop');
	}

	public function configCompany(Request $request) 
	{
		//dd('company.configCompany');
		//$company = $this->repo->update($request->toArray(), auth()->user()->id);
		//$simulation = auth()->user()->simulation;

		//dd($company,$request);

		//notification('Empresa configurada com sucesso!');

		//$company->configured = 1;
		//$company->save();
		return view('/home');
		//return view('company.index', compact('simulation'));
	}
}
