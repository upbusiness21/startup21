@extends('adminlte::page')

@section('content_header')
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <h1>Configuração Inicial da Empresa</h1>
  <style type="text/css">
	.content2{
        width: 68%;
        padding: 10px;
        margin: 1px auto;
      }
	  .button1 {
        background-color: #d5d2d9; 
        color: black; 
		border-radius: 10px;
      }
	  .button1:hover {
        background-color: #d5d2d9;
        color: #ff890a;
		border: 5px solid #ff890a;
      }
	  .button{
		  width:100px;
		  height:100px;
	  }
  </style>
@stop


@section('content')
        {!! Form::model(auth()->user(), ['route' => 'company.preparationConfig', 'method' => 'put',  'class'=>'form-horizontal', 'files' => true]) !!}
        	<div align="center">
				<form method="POST" action="" enctype="multipart/form-data">
					<input type="file" name="imagemF" id="imagemF" onchange="previewImagemF()"><br>
					<img style="width: 150px; height: 150px; background:#d5d2d9; border:#00004d solid 5px; border-radius:20px;"><br><br>
				</form>
			
				<script>
					function previewImagemF(){
						var imagemF = document.querySelector('input[name=imagemF]').files[0];
						var preview = document.querySelector('img');
						var reader = new FileReader();
						reader.onloadend = function(){
							preview.src = reader.result;
						}

						if(imagemF) {
							reader.readAsDataURL(imagemF);
						}else{
							preview.src = "";
						}
					}
				</script>
			</div>
		<!--<div class="form-group">
        	 {!! Form::label('name', 'Nome da Empresa (até 12 caracteres)', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}-->
        	  <div align="center">
				<input 
					style=
						"width:150px; 
						text-align: center; 
						background:#00004d; 
						border-radius:10px;
						color:#fff;"

					class="form-control" 
					type="text" 
					placeholder="NOME" 
					aria-label="default input example"><br>
			  </div>
			<!-- <div class="col-sm-3 input-group">
        	    <div class="input-group-addon">...</div>
        	    {!! Form::text('name', null, ['class' => 'form-control', 'required','maxlength' => 12]); !!}
        	  </div>
        	</div>-->

            <!--<div class="form-group">
              {!! Form::label('photo', 'Foto', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-3 input-group">
                <div class="input-group-addon"><i class="fa fa-camera"></i></div>
                {!! Form::file('photo', ['class' => 'form-control']); !!}
              </div>
            </div>
			<br>	-->
			<!--<div class="form-group">
				<div class="input-group col-md-offset-3">
					<h4>Selecione o produto que deseja produzir</h2>  
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '1'); !!}
						Bike
					</label>
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '2'); !!}
						TV
					</label>
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '3'); !!}
						Tablet
					</label>
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '4'); !!}
						Vídeo-Game
					</label>
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '5'); !!}
						Computador
					</label>
				</div>

			</div>-->
			<!--<div align="center">
				<h6><strong>SELECIONE O PRODUTO A SER FABRICADO</strong></h6><br>
				<button type="button" class="botn btn btn-secondary btn-lg"><span <i class="bi bi-bicycle"></i></span></button>
				<button type="button" class="botn btn btn-secondary btn-lg"></button>
				<button type="button" class="botn btn btn-secondary btn-lg"></button>
				<button type="button" class="botn btn btn-secondary btn-lg"></button>
				<button type="button" class="botn btn btn-secondary btn-lg"></button>
			</div><br><br>-->
			<div class="content2" align="center">
				<h5><strong>SELECIONE O PRODUTO A SER FABRICADO</strong></h5><br>
				<button class="button button1">BIKE</button>
				<button class="button button1">TV</button>
				<button class="button button1">TABLET</button>
				<button class="button button1">GAME</button>
				<button class="button button1">COMPIUTER</button>
			</div><br><br>

			<!--<div class="col-md-offset-5 col-md-3 text-center">
				{!! Form::submit('Seguinte', ['class' => 'btn btn-success']); !!}
			</div>-->
			<div align="center">
				<button type="button" class="btn btn-warning">CONFIRMAR</button>
			</div><br>

        {!! Form::close() !!}
@stop