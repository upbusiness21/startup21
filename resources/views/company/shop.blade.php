@extends('adminlte::page')

@section('content_header')
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <h1>Aquisições Iniciais da empresa</h1>
@stop


@section('content')
  {!! Form::model(auth()->user(), ['route' => 'company.configCompany', 'method' => 'put',  'class'=>'form-horizontal', 'files' => true]) !!}



  <div id="Purchase">
            <div class="input-group col-md-offset-2"> 
                  <img src="{{ asset('images/purchase.jpg') }}" usemap="#image-map" style="height: 40%; width: 40%; object-fit: contain">
                  <map name="image-map">
                    <!-- coords -- inicial x, inicial y , diametro do círculo-->
                    <area target="" alt="propaganda" coords="486,223,60" onclick="funcaoPropaganda();" style="cursor: pointer;" title="Agência Propaganda" shape="circle">
                    <area target="" alt="máquinas" coords="177,413,60" onclick="funcaoMaquinas();" style="cursor: pointer;" title="Máquinas da Produção" shape="circle">
                    <area target="" alt="finanças" coords="130,250,60" onclick="funcaoFinancas();" style="cursor: pointer;" title="Banco" shape="circle">
                    <area target="" alt="produção" coords="330,123,60" onclick="funcaoProducao();" style="cursor: pointer;" title="Matéria prima" shape="circle">
                    <area target="" alt="empresa" coords="520,460,80" onclick="funcaoEmpresa();" style="cursor: pointer;" title="Empresa" shape="circle">
                  </map>
            </div>
            <div class="footer">
            </div>
</div>


      <div class="col-md-offset-4 col-md-1 text-center">
      <br>
        <button type="button" class="btn btn-warning" onclick="goBack()">Voltar</button>
			</div>
			<div class="col-md-offset-0 col-md-1 text-center">
        <br>
				{!! Form::submit('Finalizar Configuração', ['class' => 'btn btn-success']); !!}
      </div>
{!! Form::close() !!}




<!--    MODAIS    --> 
<div class="modal modal-default" id="propaganda">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header modal-header-x">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">COMERCIAL - PROPAGANDA</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
              {!! Form::label('adv_radio', 'Radio', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_radio', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_journal', 'Jornal', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_journal', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_social', 'Mídias Sociais', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_social', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_outdoor', 'Outdoor', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_outdoor', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_tv', 'TV', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_tv', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success"> Ok </button>
        </div>
      </div>
    </div>
</div>
<div class="modal modal-default" id="maquinas">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header modal-header-x">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">COMPRA E VENDA LINHAS DE PRODUÇÃO</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                {!! Form::label('buy_small_machine', 'Compra P', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_s_m_id">
                  {!! Form::label('b_s_m', 'Recurso próprio (P)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_s_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('buy_medium_machine', 'Compra M', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_m_m_id">
                  {!! Form::label('b_m_m', 'Recurso próprio (M)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_m_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('buy_large_machine', 'Compra G', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_l_m_id">
                  {!! Form::label('b_l_m', 'Recurso próprio (G)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_l_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('sell_small_machine', 'Venda P', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('sell_medium_machine', 'Venda M', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('sell_large_machine', 'Venda G', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success"> Ok </button>
            </div>
          </div>
        </div>
</div>
<div class="modal modal-default" id="financas">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">FINANÇAS</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          {!! Form::label('loan', 'Empréstimo', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">$</div>
            {!! Form::text('loan', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('anticipation_of_receivables', 'Antecipação de Recebíveis', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('anticipation_of_receivables', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('application', 'Aplicação', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('application', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('term_interest', 'Juros nas vendas a prazo', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('term_interest', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success"> Ok </button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-default" id="producao">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">PROGRAMAÇÃO DA PRODUÇÃO</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-info">
           <div class="panel-heading">PERÍODO ATUAL</div>
           <div class="panel-body">
            <div class="form-group">
              {!! Form::label('emergency_rma', 'Emergencial - Quadro', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmb', 'Emergencial - Kit 1', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmc', 'Emergencial - Kit 2', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('activity_level', 'Nível de atividade', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('activity_level', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('extra_production', 'Produção extra', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('extra_production', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
           </div>
        </div>
        <div class="panel panel-warning">
          <div class="panel-heading">PRÓXIMO PERÍODO</div>
          <div class="panel-body">
            <div class="form-group">
              {!! Form::label('scheduled_rma', 'Quadro', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('scheduled_rmb', 'Kit 1', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('scheduled_rmc', 'Kit 2', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('payment_rm', 'Pagamento MP', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                {!! Form::select('payment_rm', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success"> Salvar </button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-default" id="decisao">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header modal-header-x">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">DECISÃO DA EMPRESA</h4>
        </div>
        <div class="modal-body">
          <div class="panel panel-info">
             <div class="panel-heading">COMERCIAL</div>
             <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('price', 'Preço à vista', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">$</div>
                      {!! Form::text('price', !empty($decision) ? null : 7, ['class' => 'form-control']); !!}
                    </div>
                  </div>
          
                  <div class="form-group">
                    {!! Form::label('term', 'Prazo', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                      {!! Form::select('term', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
             </div>
          </div>
          <div class="panel panel-warning">
            <div class="panel-heading">RH</div>
            <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('admitted', 'Admitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">N.</div>
                      {!! Form::text('admitted', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('fired', 'Demitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">N.</div>
                      {!! Form::text('fired', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('salary', 'Salário', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">$</div>
                      {!! Form::text('salary', !empty($decision) ? null : 7, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('training', 'Treinamento', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">%</div>
                      {!! Form::text('training', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success"> Salvar </button>
        </div>
      </div>
    </div>
  </div>
{{--
<div class="modal modal-default" id="decisao">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">DECISÃO DA EMPRESA: RODADA 03</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-info">
          <div class="panel-heading">COMERCIAL</div>
            <div class="panel-body">
        
        
        
         
         
            </div>
          </div>
        </div>
        <div class="panel panel-info">
          <div class="panel-heading">COMERCIAL</div>
            <div class="panel-body">
          
          
           
           
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success"> Salvar </button>
      </div>
    </div>
  </div>
</div> --}}





@stop
@section('js')
<script>
  function goBack() {
    window.history.back();
  }

  $(document).ready(function(e) {
        $('img[usemap]').rwdImageMaps();
    });
    $('#Praca').on('shown.bs.modal', function (e) {
      $('img[usemap]').rwdImageMaps();
    })
    function ShowPraca() {
      $('#Praca').modal('show');
    }
    function funcaoPropaganda() {
      $('#propaganda').modal();
    }
    function funcaoMaquinas() {
      $('#maquinas').modal();
    }
    function funcaoFinancas() {
	    $('#financas').modal();
    }
    function funcaoProducao() {
      $('#producao').modal();
    }
    function funcaoEmpresa() {
      $('#decisao').modal();
    }
    function teste() {
       alert('teste kjkjka');
    }
</script>
<script>
  $(function() {

    $("#decision").submit(function(e) {
      e.stopPropagation();

      var $bt = $(this).find(':submit');

      $bt.attr('disabled', 'disabled');
      $bt.val('Gravando...');
      $bt.css('cursor', 'progress');
    });

    $('[id*=adv_]').change(function(e) {
      if ($(this).val() == null) {
        $(this).val(0);
      }
    })

    if ($('#buy_small_machine').val() > 0) {
      $('#b_s_m_id').removeClass('hidden');
    }
    if ($('#buy_medium_machine').val() > 0) {
      $('#b_m_m_id').removeClass('hidden');
    }
    if ($('#buy_large_machine').val() > 0) {
      $('#b_l_m_id').removeClass('hidden');
    }

    $('#buy_small_machine, #buy_medium_machine, #buy_large_machine').change(function() {
      let val = $(this).val();
      let id = $(this).attr('id');
      if (val != 0) {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').removeClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').removeClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').removeClass('hidden');
        }
      } else {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').addClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').addClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').addClass('hidden');
        }
      }
    });
  })
</script>
@stop




